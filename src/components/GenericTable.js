import React, { Fragment, useEffect, useState } from "react";
import { Link, withRouter, useHistory } from "react-router-dom";
import { Container, TableWrapper, ConfirmWrapper } from "./styles";
import api from "../services/api";
import { usePagination, useTable } from "react-table";
import { AppTitle } from "../styles/genericStyles";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import GenericForm from "./GenericForm/GenericForm";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const swal = withReactContent(Swal);

function Table({ columns, data }) {
	// Use the state and functions returned from useTable to build your UI
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		prepareRow,
		canPreviousPage,
		canNextPage,
		pageOptions,
		pageCount,
		gotoPage,
		nextPage,
		previousPage,
		setPageSize,
		state: { pageIndex, pageSize },
	} = useTable(
		{
			columns,
			data,
			initialState: { pageIndex: 0 },
		},
		usePagination
	);

	// Render the UI for your table
	return (
		<>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup) => (
						<tr {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column) => (
								<th {...column.getHeaderProps()}>{column.render("Header")}</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row);
						return (
							<tr {...row.getRowProps()}>
								{row.cells.map((cell) => {
									return (
										<td {...cell.getCellProps()}>{cell.render("Cell")}</td>
									);
								})}
							</tr>
						);
					})}
				</tbody>
			</table>
			{/* 
        Pagination can be built however you'd like. 
        This is just a very basic UI implementation:
      */}
			<div className="pagination">
				<div></div>
				<div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-left"}
							onClick={() => previousPage()}
							disabled={!canPreviousPage}
						></button>{" "}
					</div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-right"}
							onClick={() => nextPage()}
							disabled={!canNextPage}
						></button>{" "}
					</div>
				</div>
				<div>
					<span className="paginationText">
						Página{" "}
						<strong>
							{pageIndex + 1} de {pageOptions.length}
						</strong>{" "}
					</span>
				</div>
			</div>
		</>
	);
}

const GenericTable = (props) => {
	const [data, setData] = useState([]);
	const [confirmOpened, setconfirmOpened] = useState(false);
	const [isOnForm, setisOnForm] = useState(false);
	const [isEdit, setisEdit] = useState({ edit: false, id: null });
	const history = useHistory();
	let modalOpened = false;

	useEffect(async () => {
		setconfirmOpened(modalOpened);
		if (data.length == 0 && props.config) {
			const result = await request();
			setData(result);
		}
	}, [data, modalOpened, isOnForm]);

	const request = async () => {
		let data = [];
		let obj = {};

		await api.get(props.config.service).then((r) => {
			r.data.map((i) => {
				props.config.columns.map((c) => {
					let string = c.accessor;
					obj[string] = i[string];
				});
				data.push(obj);
				obj = {};
			});
		});
		return data;
	};

	const deleteItem = async (id) => {
		try {
			api
				.delete(`${props.config.service}?id=${id}`)
				.then((r) => {
					setData(data.filter((i) => i.id_usuario != id));
					swal.fire({
						title: "Excluído!",
						icon: "success",
						timer: 3000,
						toast: true,
						position: "top-end",
						showConfirmButton: false,
					});
					setconfirmOpened(false);
				})
				.catch((err) => {
					swal.fire({
						title: "Item possui vínculos!",
						icon: "error",
						timer: 3000,
						toast: true,
						position: "top-end",
						showConfirmButton: false,
					});
					setconfirmOpened(false);
				});
		} catch (e) {
			swal.fire({
				title: "Item possui vínculos!",
				icon: "error",
				timer: 3000,
				toast: true,
				position: "top-end",
				showConfirmButton: false,
			});
		}
	};

	const confirmDelete = async (id) => {
		const confirmOptions = {
			closeOnClickOutside: false,
			customUI: ({ onClose }) => {
				return (
					<ConfirmWrapper>
						<div>
							<h2>Tem certeza?</h2>
						</div>
						<div>
							<p>Você deseja excluir este item?</p>
						</div>
						<div className="buttonsWrapper">
							<button
								onClick={() => {
									onClose();
									deleteItem(id);
								}}
							>
								Sim
							</button>
							<button
								onClick={() => {
									onClose();
									setconfirmOpened(false);
								}}
							>
								Não
							</button>
						</div>
					</ConfirmWrapper>
				);
			},
		};
		setconfirmOpened(true);
		confirmAlert(confirmOptions);
	};

	const edit = (id) => {
		if (props.config.service == "tarefa") history.push("editaTarefa/" + id);
		setisEdit({ edit: true, id: id });
		setisOnForm(true);
	};

	const MakeColumns = () => {
		let cols = props.config.columns;
		let id = "id";
		if (props.config.service == "usuario") id = "id_usuario";
		if (props.config.service == "sistema") id = "id_sistema";
		cols = cols.filter((item) => item.Header != "Id");
		cols.push({
			Header: "Ações",
			id: "actions",
			accessor: id,
			Cell: ({ value }) => (
				<div>
					<button
						className={"iconButton button is-white fa fa-edit edit"}
						onClick={(e) => edit(value)}
					></button>
					<button
						className={"iconButton button is-white fa fa-trash delete"}
						onClick={(e) => confirmDelete(value)}
					></button>
				</div>
			),
		});
		return cols;
	};

	const newItem = () => {
		if (props.config.service == "tarefa") history.push("createTarefa");
		setisOnForm(true);
	};

	const exitForm = () => {
		setisEdit({ ...isEdit, ...{ edit: false } });
		setData([]);
		setisOnForm(false);
	};
	const columns = React.useMemo(() => MakeColumns(), []);

	return (
		<Container confirmOpened={confirmOpened}>
			{!isOnForm ? (
				<Fragment>
					<Link style={{ textDecoration: "none" }} to="/menu">
						<p className="whiteText">Voltar</p>
					</Link>
					<AppTitle>{props.config.texts.title}</AppTitle>
					<button id="newItemButton" onClick={newItem}>
						+ {props.config.texts.buttonText}
					</button>
					<TableWrapper styles={props.config.styles}>
						{!data.length ? (
							<div className="loadingText">Carregando...</div>
						) : (
							<Table columns={columns} data={data} />
						)}
					</TableWrapper>
				</Fragment>
			) : (
				<Fragment>
					<GenericForm
						config={props.config}
						edit={isEdit}
						onFormExit={exitForm}
					/>
				</Fragment>
			)}
		</Container>
	);
};

export default GenericTable;
