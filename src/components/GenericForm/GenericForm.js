import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { Container, FormWrapper } from "./styles";
import api from "../../services/api";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { set } from "date-fns/esm";

const swal = withReactContent(Swal);

const GenericForm = (props) => {
	const [hasEmptyFields, sethasEmptyFields] = useState(true);
	const [body, setBody] = useState({
		id_criador: localStorage.getItem("user"),
	});
	const [edit, setEdit] = useState({});
	const [itemEdit, setItemEdit] = useState({});
	const [startDate, setStartDate] = useState(new Date());
	const [flag, setFlag] = useState(0);
	let fields = [];
	useEffect(async () => {
		if (
			props.edit.edit &&
			itemEdit.id == undefined &&
			itemEdit.id_usuaruo == undefined &&
			flag == 0
		) {
			setEdit(props.edit);
			let result = await getItemEdit();
			setItemEdit(result);
			setFlag(1);
		}
		fields = createForm();
	}, [itemEdit, props.edit]);

	const getItemEdit = async () => {
		let data = {};
		await api
			.get(`${props.config.service}?id=${props.edit.id}`)
			.then((response) => {
				data = response.data[0];
			});

		return data;
	};

	const putItem = async () => {
		let corpo = body;
		corpo["id"] = itemEdit.id || itemEdit.id_usuario || itemEdit.id_sistema;
		let route = props.config.servicePost || props.config.service;
		try {
			api.put(route + "?id=" + corpo.id, corpo).then(
				(response) => {
					swal.fire({
						title: "Sucesso!",
						icon: "success",
						timer: 3000,
						toast: true,
						position: "top-end",
						showConfirmButton: false,
					});
				},
				(error) => {}
			);
		} catch (e) {
			swal.fire({
				title: "Erro!",
				icon: "error",
				timer: 3000,
				toast: true,
				position: "top-end",
				showConfirmButton: false,
			});
		}
	};

	const createItem = async () => {
		let route = props.config.servicePost || props.config.service;

		try {
			api.post(route, body).then(
				(response) => {
					swal.fire({
						title: "Sucesso!",
						icon: "success",
						timer: 3000,
						toast: true,
						position: "top-end",
						showConfirmButton: false,
					});
				},
				(error) => {}
			);
		} catch (e) {
			swal.fire({
				title: "Erro!",
				icon: "error",
				timer: 3000,
				toast: true,
				position: "top-end",
				showConfirmButton: false,
			});
		}
	};

	const checkIfEmptyFields = async () => {
		let fields = props.config.fields;
		let emptyFields = false;
		fields.map((field) => {
			if (edit) {
				if (body[field.value] == "" || body[field.value] == "default") {
					emptyFields = false;
				}
			} else if (
				!body[field.value] ||
				body[field.value] == "" ||
				body[field.value] == "default"
			) {
				emptyFields = true;
			}
		});

		sethasEmptyFields(emptyFields);
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		if (props.config.service === "projeto")
			setBody({ ...body, ...{ id_criador: localStorage.getItem("user") } });
		if (props.edit.edit) await putItem();
		else await createItem();
	};

	const handleChange = async (e, data = null) => {
		let aux = body;
		if (data) aux[e] = data;
		else aux[e.target.name] = e.target.value;
		setBody(aux);
		await checkIfEmptyFields();
	};

	const createTextField = (field, key) => {
		return (
			<div className="fieldWrapper" key={key}>
				<label>
					<p>{field.label}</p>
				</label>
				<input
					autoComplete="off"
					className="formField"
					onChange={(e) => handleChange(e)}
					onBlur={(e) => handleChange(e)}
					type="text"
					name={field.value}
					defaultValue={edit.edit ? itemEdit[field.value] : ""}
				/>
			</div>
		);
	};

	const generateOptions = (field) => {
		let options = [];
		let count = 1;
		let defaultOption =
			props.edit == true ? (
				<option key={0} value="default">
					Selecione...
				</option>
			) : (
				<option key={0} selected="selected" value="default">
					Selecione...
				</option>
			);
		options.push(defaultOption);
		field.options.map((option, key) => {
			if (props.edit.edit == true && option[key] == itemEdit[key]) {
				options.push(
					<option selected="selected" key={count} value={option.id}>
						{option.value}
					</option>
				);
			} else {
				options.push(
					<option key={count} value={option.id}>
						{option.value}
					</option>
				);
			}

			count++;
		});

		return options;
	};

	const exitForm = () => {
		// let aux = body;
		// Object.keys(aux).forEach(function (key) {
		// 	delete aux[key];
		// });
		setItemEdit({});
		setBody({});
		props.onFormExit();
	};

	const createSelectField = (field, key) => {
		return (
			<div className="fieldWrapper" key={key}>
				<label>
					<p>{field.label}</p>
				</label>
				<select
					// defaultValue={edit.edit ? itemEdit[field.value] : "default"}
					className="formField"
					name={field.value}
					onChange={(e) => handleChange(e)}
					onBlur={(e) => handleChange(e)}
				>
					{generateOptions(field, field.value)}
				</select>
			</div>
		);
	};

	const createDatepicker = (field, key) => {
		return (
			<div className="fieldWrapper" key={key}>
				<DatePicker
					selected={startDate}
					onChange={(date, e) => {
						let data = moment(date).format("DD-MM-YYYY HH:MM:SS");
						handleChange(field.value, data);
					}}
				/>
			</div>
		);
	};

	const createForm = () => {
		let form = [];
		let key = 0;
		props.config.fields.map((f) => {
			if (f.type == "text") form.push(createTextField(f, key));
			if (f.type == "select") form.push(createSelectField(f, key));
			if (f.type == "date") form.push(createDatepicker(f, key));
			key++;
		});

		return form;
	};

	fields = createForm();

	return (
		<form onSubmit={handleSubmit}>
			<Container>
				<FormWrapper>{fields}</FormWrapper>
				<input
					disabled={hasEmptyFields}
					id="submitButton"
					type="submit"
					value="Criar"
				/>
				<button id="voltar" onClick={() => exitForm()}>
					Voltar
				</button>
			</Container>
		</form>
	);
};

export default GenericForm;
