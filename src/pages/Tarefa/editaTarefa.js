import React, { forwardRef, useEffect, useState } from "react";
import { Link, useParams, withRouter } from "react-router-dom";
import api from "../../services/api";
import { Container, FormWrapper } from "../../pages/Tarefa/styles";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import ptBR from "date-fns/locale/pt-BR";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const swal = withReactContent(Swal);
registerLocale("pt-BR", ptBR);

const fields = [
	{
		required: true,
		value: "titulo",
	},
	{
		value: "descricao",
	},
	{
		required: true,
		value: "id_projeto",
	},
	{
		value: "id_pai_tarefa",
	},

	{
		value: "id_tipo_tarefa",
	},

	{
		value: "id_status_tarefa",
	},

	{
		value: "tempo_estimado",
	},
	{
		required: true,
		value: "complexidade",
	},
	{
		required: true,
		value: "impacto",
	},
	{
		value: "id_criador",
	},
	{
		value: "id_dev",
	},
	{
		required: true,
		value: "id_prioridade",
	},
	{
		value: "id_grupo",
	},
	{
		value: "inicio_dev",
	},
	{
		value: "fim_dev",
	},
];

const EditaTarefa = (props) => {
	const [edit, setEdit] = useState(false);
	const [tarefas, setTarefas] = useState([]);
	const [prioridades, setPrioridades] = useState([]);
	const [tipoTarefa, setTipoTarefa] = useState([]);
	const [statusTarefa, setStatusTarefa] = useState([]);
	const [grupo, setGrupo] = useState([]);
	const [usuario, setUsuario] = useState([]);
	const [projetos, setProjetos] = useState([]);
	const [hasEmptyFields, sethasEmptyFields] = useState(true);
	const [inicioDev, setInicioDev] = useState(new Date());
	const [fimDev, setFimDev] = useState(new Date());
	const [inicio, setInicio] = useState(new Date());
	const [flag, setFlag] = useState(0);
	const [itemEdit, setItemEdit] = useState({});
	const [body, setBody] = useState({
		inicio_dev: moment(new Date()).format("DD-MM-YYYY HH:MM:SS"),
		fim_dev: moment(new Date()).format("DD-MM-YYYY HH:MM:SS"),
		inicio: moment(new Date()).format("DD-MM-YYYY HH:MM:SS"),
		id_criador: localStorage.getItem("user"),
	});
	const {
		match: { params },
	} = props;

	const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (
		<button className="formField" onClick={onClick} ref={ref}>
			{value}
		</button>
	));

	useEffect(async () => {
		if (itemEdit.id == undefined && flag == 0) {
			let result = await getItem();
			setItemEdit(result);
			setFlag(1);
		} else {
			let merge = { ...body, ...itemEdit };
			setBody(merge);
			return;
		}
		if (!tarefas || tarefas.length == 0) {
			const result = await getTarefas();
			setTarefas(result);
		}
		if (!tipoTarefa || tipoTarefa.length == 0) {
			const result = await getTipoTarefas();
			setTipoTarefa(result);
		}
		if (!usuario || usuario.length == 0) {
			const result = await getUsuario();
			setUsuario(result);
		}
		if (!statusTarefa || statusTarefa.length == 0) {
			const result = await getStatusTarefas();
			setStatusTarefa(result);
		}
		if (!projetos || projetos.length == 0) {
			const result = await getProjetos();
			setProjetos(result);
		}
		if (!prioridades || prioridades.length == 0) {
			const result = await getPrioridades();
			setPrioridades(result);
		}
		if (!grupo || grupo.length == 0) {
			const result = await getGrupos();
			setGrupo(result);
		}
	}, [itemEdit]);

	const getItem = async () => {
		let data = {};
		await api.get(`tarefa?id=${params.id}`).then((response) => {
			data = response.data[0];
		});

		return data;
	};

	const getGrupos = async () => {
		let data = [];
		await api.get("grupo").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getUsuario = async () => {
		let data = [];
		await api.get("usuario").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id_usuario, value: t.nome });
			});
		});
		return data;
	};

	const getProjetos = async () => {
		let data = [];
		await api.get("projeto").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getTarefas = async () => {
		let data = [];
		await api.get("tarefa").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.tarefa });
			});
		});
		return data;
	};

	const getStatusTarefas = async () => {
		let data = [];
		await api.get("tarefa_status").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getTipoTarefas = async () => {
		let data = [];
		await api.get("tarefa_tipo").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getPrioridades = async () => {
		let data = [];
		await api.get("prioridade").then((r) => {
			r.data.map((i) => {
				data.push({ id: i.id, value: i.descricao });
			});
		});
		return data;
	};

	const handleChange = async (e, data = null) => {
		let aux = body;
		if (data) aux[e] = data;
		else aux[e.target.name] = e.target.value;
		setBody(aux);
		await checkIfEmptyFields();
	};

	const checkIfEmptyFields = async () => {
		let campos = fields;
		let emptyFields = false;
		campos.map((field) => {
			if (field.required) {
				if (
					!body[field.value] ||
					body[field.value] == "" ||
					body[field.value] == "default"
				) {
					emptyFields = true;
					return;
				}
			}
		});

		sethasEmptyFields(emptyFields);
	};

	const generateOptions = (field, selectedValue) => {
		let options = [];
		let count = 1;
		options.push(
			<option key={0} value="default">
				Selecione...
			</option>
		);
		field.map((option) => {
			if (selectedValue == option.id) {
				options.push(
					<option selected={true} key={count} value={option.id}>
						{option.value}
					</option>
				);
			} else {
				options.push(
					<option key={count} value={option.id}>
						{option.value}
					</option>
				);
			}
			count++;
		});

		return options;
	};

	const createItem = async () => {
		try {
			api.put("tarefa?id=" + itemEdit.id, body).then(
				(response) => {
					swal.fire({
						title: "Sucesso!",
						icon: "success",
						timer: 3000,
						toast: true,
						position: "top-end",
						showConfirmButton: false,
					});
				},
				(error) => {
				}
			);
		} catch (e) {
			alert("Um erro ocorreu!");
		}
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		await createItem();
	};

	return (
		<div>
			{itemEdit.id != undefined ? (
				<form onSubmit={handleSubmit}>
					<Container>
						<FormWrapper>
							<div className="fieldWrapper" key={0}>
								<label>
									<p>Título</p>
								</label>
								<input
									autoComplete="off"
									className="formField"
									onChange={(e) => handleChange(e)}
									type="text"
									name="titulo"
									defaultValue={itemEdit["titulo"]}
								/>
							</div>
							<div className="fieldWrapper" key={1}>
								<label>
									<p>Descrição</p>
								</label>
								<input
									autoComplete="off"
									className="formField"
									onChange={(e) => handleChange(e)}
									type="text"
									name="descricao"
									defaultValue={itemEdit["descricao"]}
								/>
							</div>
							<div className="fieldWrapper" key={2}>
								<label>
									<p>Tempo Estimado</p>
								</label>
								<input
									autoComplete="off"
									className="formField"
									onChange={(e) => handleChange(e)}
									type="number"
									name="tempo_estimado"
									defaultValue={itemEdit["tempo_estimado"]}
								/>
							</div>
							<div className="fieldWrapper" key={3}>
								<label>
									<p>Prioridade</p>
								</label>
								<select
									className="formField"
									name="id_prioridade"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(prioridades, itemEdit["id_prioridade"])}
								</select>
							</div>
							<div className="fieldWrapper" key={4}>
								<label>
									<p>Tarefa pai</p>
								</label>
								<select
									className="formField"
									name="id_pai_tarefa"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(tarefas, itemEdit["id_pai_tarefa"])}
								</select>
							</div>
							<div className="fieldWrapper" key={5}>
								<label>
									<p>Status da tarefa</p>
								</label>
								<select
									className="formField"
									name="id_status_tarefa"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(statusTarefa, itemEdit["id_status_tarefa"])}
								</select>
							</div>
							<div className="fieldWrapper" key={6}>
								<label>
									<p>Tipo da tarefa</p>
								</label>
								<select
									className="formField"
									name="id_tipo_tarefa"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(tipoTarefa, itemEdit["id_tipo_tarefa"])}
								</select>
							</div>
							<div className="fieldWrapper" key={7}>
								<label>
									<p>Desenvolvedor</p>
								</label>
								<select
									className="formField"
									name="id_dev"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(usuario, itemEdit["id_dev"])}
								</select>
							</div>
							<div className="fieldWrapper" key={8}>
								<label>
									<p>Grupo</p>
								</label>
								<select
									className="formField"
									name="id_grupo"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(grupo, itemEdit["id_grupo"])}
								</select>
							</div>
							<div className="fieldWrapper" key={9}>
								<label>
									<p>Início de desenvolvimento</p>
								</label>
								<DatePicker
									locale="pt-BR"
									customInput={<ExampleCustomInput />}
									dateFormat="dd/MM/yyyy"
									selected={inicioDev}
									onChange={(date, e) => {
										setInicioDev(date);
										let data = moment(date).format("DD-MM-YYYY HH:MM:SS");
										handleChange("inicio_dev", data);
									}}
								/>
							</div>
							<div className="fieldWrapper" key={10}>
								<label>
									<p>Fim de desenvolvimento</p>
								</label>
								<DatePicker
									locale="pt-BR"
									customInput={<ExampleCustomInput />}
									dateFormat="dd/MM/yyyy"
									selected={fimDev}
									onChange={(date, e) => {
										setFimDev(date);
										let data = moment(date).format("DD-MM-YYYY HH:MM:SS");
										handleChange("fim_dev", data);
									}}
								/>
							</div>
							<div className="fieldWrapper" key={11}>
								<label>
									<p>Iniciado em</p>
								</label>
								<DatePicker
									locale="pt-BR"
									customInput={<ExampleCustomInput />}
									dateFormat="dd/MM/yyyy"
									selected={inicio}
									onChange={(date, e) => {
										setInicio(date);
										let data = moment(date).format("DD-MM-YYYY HH:MM:SS");
										handleChange("iniciado", data);
									}}
								/>
							</div>
							<div className="fieldWrapper" key={12}>
								<label>
									<p>Complexidade</p>
								</label>
								<input
									autoComplete="off"
									className="formField"
									onChange={(e) => handleChange(e)}
									type="number"
									name="complexidade"
									defaultValue={itemEdit["complexidade"]}
								/>
							</div>
							<div className="fieldWrapper" key={13}>
								<label>
									<p>Projeto</p>
								</label>
								<select
									className="formField"
									name="id_projeto"
									onChange={(e) => handleChange(e)}
								>
									{generateOptions(projetos, itemEdit["id_projeto"])}
								</select>
							</div>
							<div className="fieldWrapper" key={14}>
								<label>
									<p>Impacto</p>
								</label>
								<input
									autoComplete="off"
									className="formField"
									onChange={(e) => handleChange(e)}
									type="number"
									name="impacto"
									defaultValue={itemEdit["impacto"]}
								/>
							</div>
						</FormWrapper>
						<input
							disabled={hasEmptyFields}
							id="submitButton"
							type="submit"
							value="Editar"
						/>
						<Link style={{ textDecoration: "none" }} to="/tarefa">
							<p style={{ marginTop: "20px" }}>Voltar</p>
						</Link>
					</Container>
				</form>
			) : (
				<p style={{ paddingTop: "20px" }}>loading</p>
			)}
		</div>
	);
};

export default withRouter(EditaTarefa);
