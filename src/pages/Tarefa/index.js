import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const Tarefa = () => {
	const [tarefas, setTarefas] = useState([]);
	const [prioridades, setPrioridades] = useState([]);
	const [tipoTarefa, setTipoTarefa] = useState([]);
	const [statusTarefa, setStatusTarefa] = useState([]);
	const [grupo, setGrupo] = useState([]);
	const [usuario, setUsuario] = useState([]);
	const [projetos, setProjetos] = useState([]);

	useEffect(async () => {
		if (!tarefas || tarefas.length == 0) {
			const result = await getTarefas();
			setTarefas(result);
		}
		if (!tipoTarefa || tipoTarefa.length == 0) {
			const result = await getTipoTarefas();
			setTipoTarefa(result);
		}
		if (!usuario || usuario.length == 0) {
			const result = await getUsuario();
			setUsuario(result);
		}
		if (!statusTarefa || statusTarefa.length == 0) {
			const result = await getStatusTarefas();
			setStatusTarefa(result);
		}
		if (!projetos || projetos.length == 0) {
			const result = await getProjetos();
			setProjetos(result);
		}
		if (!prioridades || prioridades.length == 0) {
			const result = await getPrioridades();
			setPrioridades(result);
		}
		if (!grupo || grupo.length == 0) {
			const result = await getGrupos();
			setGrupo(result);
		}
	}, [tarefas, prioridades, tipoTarefa, statusTarefa, grupo, usuario]);

	const getGrupos = async () => {
		let data = [];
		await api.get("grupo").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getUsuario = async () => {
		let data = [];
		await api.get("usuario").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id_usuario, value: t.nome });
			});
		});
		return data;
	};

	const getProjetos = async () => {
		let data = [];
		await api.get("projeto").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getTarefas = async () => {
		let data = [];
		await api.get("tarefa").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.tarefa });
			});
		});
		return data;
	};

	const getStatusTarefas = async () => {
		let data = [];
		await api.get("tarefa_status").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getTipoTarefas = async () => {
		let data = [];
		await api.get("tarefa_tipo").then((r) => {
			r.data.map((t) => {
				data.push({ id: t.id, value: t.descricao });
			});
		});
		return data;
	};

	const getPrioridades = async () => {
		let data = [];
		await api.get("prioridade").then((r) => {
			r.data.map((i) => {
				data.push({ id: i.id, value: i.descricao });
			});
		});
		return data;
	};

	const styles = { width: "95vw" };

	const columns = [
		{
			Header: "Tarefa",
			accessor: "tarefa", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
		{
			Header: "Tipo de tarefa",
			accessor: "tarefa_tipo", // accessor is the "key" in the data
		},
		{
			Header: "Status de tarefa",
			accessor: "tarefa_status", // accessor is the "key" in the data
		},
		{
			Header: "Criado em",
			accessor: "fim",
		},
		{
			Header: "Duração",
			accessor: "duracao_estimada",
		},
		{
			Header: "Grupo",
			accessor: "grupo",
		},
		{
			Header: "Desenvolvedor",
			accessor: "desenvolvedor",
		},
	];

	const texts = {
		title: "Tarefas",
		buttonText: "Nova tarefa",
	};

	const fields = [
		{
			type: "text",
			label: "Título",
			value: "titulo",
		},
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
		{
			type: "select",
			label: "Projeto",
			value: "id_projeto",
			options: projetos,
		},
		{
			type: "select",
			label: "Tarefa pai",
			value: "id_pai_tarefa",
			options: tarefas,
		},

		{
			type: "select",
			label: "Tipo tarefa",
			value: "id_tipo_tarefa",
			options: tipoTarefa,
		},

		{
			type: "select",
			label: "Status tarefa",
			value: "id_status_tarefa",
			options: statusTarefa,
		},

		{
			type: "text",
			label: "Tempo estimado",
			value: "tempo_estimado",
		},
		{
			type: "text",
			label: "Complexidade",
			value: "complexidade",
		},
		{
			type: "text",
			label: "Impacto",
			value: "impacto",
			options: prioridades,
		},
		{
			type: "select",
			label: "Usuario",
			value: "id_criador",
			options: usuario,
		},
		{
			type: "select",
			label: "Desenvolvedor",
			value: "id_dev",
			options: usuario,
		},
		{
			type: "select",
			label: "Prioridade",
			value: "id_prioridade",
			options: prioridades,
		},
		{
			type: "select",
			label: "Grupos",
			value: "id_grupo",
			options: grupo,
		},
		{
			type: "date",
			label: "Início desenvolvimento",
			value: "inicio_dev",
		},
		{
			type: "date",
			label: "fim desenvolvimento",
			value: "fim_dev",
		},
	];

	const config = {
		service: "tarefa",
		columns: columns,
		fields: fields,
		texts: texts,
		styles: styles,
	};

	return (
		<div>
			{tarefas ? <GenericTable config={config} /> : <div>carregando</div>}
		</div>
	);
};

export default withRouter(Tarefa);
