import React from "react";
import { withRouter } from "react-router-dom";
import GenericTable from "../../components/GenericTable";

const Sistema = () => {
	const columns = [
		{
			Header: "Nome",
			accessor: "nome", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id_sistema",
		},
	];

	const texts = {
		title: "Sistema",
		buttonText: "Novo sistema",
	};

	const fields = [
		{
			type: "text",
			label: "Nome",
			value: "nome",
		},
	];

	const config = {
		service: "sistema",
		columns: columns,
		fields: fields,
		texts: texts,
	};

	return <GenericTable config={config} />;
};

export default withRouter(Sistema);
