import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const StatusTarefa = () => {
	const columns = [
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Status de tarefa",
		buttonText: "Novo status",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
	];

	const config = {
		service: "tarefa_status",
		columns: columns,
		fields: fields,
		texts: texts,
	};

	return <GenericTable config={config} />;
};

export default withRouter(StatusTarefa);
