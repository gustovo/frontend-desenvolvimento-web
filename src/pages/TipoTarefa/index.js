import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../../services/api";
import GenericTable from "../../components/GenericTable";

const TipoTarefa = () => {
	const columns = [
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Tipo de tarefa",
		buttonText: "Novo tipo",
	};

	const fields = [
		{
			type: "text",
			label: "Descrição",
			value: "descricao",
		},
	];

	const config = {
		service: "tarefa_tipo",
		columns: columns,
		fields: fields,
		texts: texts,
	};

	return <GenericTable config={config} />;
};

export default withRouter(TipoTarefa);
