import React, { useEffect, useState, Fragment } from "react";
import { withRouter, Link } from "react-router-dom";
import api from "../../services/api";
import { GenericTable } from "../../components/GenericTable";
import { ContainerForm } from "./styles";
import { usePagination, useTable } from "react-table";
import { AppTitle } from "../../styles/genericStyles";
import { TableWrapper, Container } from "../../components/styles";
import ModalUsuarios from "./modalUsuarios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const swal = withReactContent(Swal);

function Table({ columns, data }) {
	// Use the state and functions returned from useTable to build your UI
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		prepareRow,
		canPreviousPage,
		canNextPage,
		pageOptions,
		pageCount,
		gotoPage,
		nextPage,
		previousPage,
		setPageSize,
		state: { pageIndex, pageSize },
	} = useTable(
		{
			columns,
			data,
			initialState: { pageIndex: 0 },
		},
		usePagination
	);

	// Render the UI for your table
	return (
		<>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup) => (
						<tr {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column) => (
								<th {...column.getHeaderProps()}>{column.render("Header")}</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row);
						return (
							<tr {...row.getRowProps()}>
								{row.cells.map((cell) => {
									return (
										<td {...cell.getCellProps()}>{cell.render("Cell")}</td>
									);
								})}
							</tr>
						);
					})}
				</tbody>
			</table>
			{/* 
        Pagination can be built however you'd like. 
        This is just a very basic UI implementation:
      */}
			<div className="pagination">
				<div></div>
				<div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-left"}
							onClick={() => previousPage()}
							disabled={!canPreviousPage}
						></button>{" "}
					</div>
					<div className="arrowWrapper">
						<button
							className={"button is-white fa fa-arrow-right"}
							onClick={() => nextPage()}
							disabled={!canNextPage}
						></button>{" "}
					</div>
				</div>
				<div>
					<span className="paginationText">
						Página{" "}
						<strong>
							{pageIndex + 1} de {pageOptions.length}
						</strong>{" "}
					</span>
				</div>
			</div>
		</>
	);
}

const ProjetoUsuario = () => {
	const [usuario, setUsuario] = useState([]);
	const [projeto, setProjeto] = useState([]);
	const [modal, setModal] = useState(false);
	const [data, setData] = useState([]);
	const [users, setUsers] = useState([]);
	const [body, setBody] = useState({});
	const [onForm, setOnForm] = useState(false);
	const [hasEmptyFields, sethasEmptyFields] = useState(true);

	useEffect(() => {
		const fetch = async () => {
			await request().then((p) => {
				setData(p);
			});
			await getUsuario().then((p) => {
				setUsuario(p);
			});
			await getProjeto().then((p) => {
				setProjeto(p);
			});
		};
		fetch();
	}, []);

	const getUsuario = async () => {
		let data = [];
		await api.get("/usuario").then((r) => {
			let response = r.data;
			response.map((r) => {
				return data.push({ id: r.id_usuario, value: r.nome });
			});
		});

		return data;
	};

	const getProjeto = async () => {
		let data = [];
		await api.get("/projeto").then((r) => {
			let response = r.data;
			response.map((r) => {
				return data.push({ id: r.id, value: r.projeto });
			});
		});

		return data;
	};

	const request = async () => {
		let data = [];
		let obj = {};

		await api.get(config.service).then((r) => {
			r.data.map((i) => {
				data.push(i);
			});
		});
		return data;
	};

	const onCloseModal = () => {
		setModal(false);
	};

	const getUsersFromProject = async (value) => {
		let data = [];
		await api.get(`/projeto_usuario?id_projeto=${value}`).then((r) => {
			data = r.data;
		});

		return data;
	};

	const openModal = async (id) => {
		let result = await getUsersFromProject(id);
		setUsers(result);
		setModal(true);
	};

	const checkIfEmptyFields = async () => {
		let fields = [
			{
				value: "id_usuario",
			},
			{
				value: "id_projeto",
			},
		];
		let emptyFields = false;
		fields.map((field) => {
			if (
				!body[field.value] ||
				body[field.value] == "" ||
				body[field.value] == "default"
			) {
				emptyFields = true;
			}
		});

		sethasEmptyFields(emptyFields);
	};

	const cols = [
		{
			Header: "Título",
			accessor: "projeto", // accessor is the "key" in the data
		},
		{
			Header: "Descrição",
			accessor: "descricao", // accessor is the "key" in the data
		},
		{
			Header: "Id",
			accessor: "id",
		},
	];

	const texts = {
		title: "Projetos",
		buttonText: "Nova relação",
	};

	const fields = [
		{
			type: "select",
			label: "Usuário",
			value: "id_usuario",
			options: usuario,
		},
		{
			type: "select",
			label: "Projeto",
			value: "id_projeto",
			options: projeto,
		},
	];

	const config = {
		service: "projeto",
		servicePost: "projeto_usuario",
		columns: cols,
		fields: fields,
		texts: texts,
		styles: {
			width: "50vw",
		},
	};

	const newItem = () => {
		setOnForm(true);
	};

	const create = async (e) => {
		e.preventDefault();
		console.log(body);
		await api.post("projeto_usuario", body).then((response) => {
			swal.fire({
				title: "Sucesso!",
				icon: "success",
				timer: 3000,
				toast: true,
				position: "top-end",
				showConfirmButton: false,
			});
		});
	};

	const handleChange = async (e) => {
		console.log(e.target.value);
		let aux = body;
		aux[e.target.name] = e.target.value;
		setBody(aux);
		console.log(e.target.name, body);
		await checkIfEmptyFields();
	};

	const generateOptions = (field) => {
		let options = [];
		let count = 1;
		let defaultOption = (
			<option key={0} selected="selected" value="default">
				Selecione...
			</option>
		);
		options.push(defaultOption);
		field.map((option, key) => {
			options.push(
				<option key={count} value={option.id}>
					{option.value}
				</option>
			);
			count++;
		});

		return options;
	};

	const MakeColumns = () => {
		let cols = config.columns;
		let id = "id";

		cols = cols.filter((item) => item.Header != "Id");
		cols.push({
			Header: "Ações",
			id: "actions",
			accessor: "id",
			Cell: ({ value }) => (
				<div>
					<button
						className={"iconButton button is-white fa fa-user user"}
						onClick={(e) => openModal(value)}
					></button>
				</div>
			),
		});

		return cols;
	};

	const columns = React.useMemo(() => MakeColumns(), []);

	return (
		<>
			<Container>
				{onForm ? (
					<Container>
						<ContainerForm>
							<p
								className="whiteText"
								onClick={() => {
									setOnForm(false);
								}}
							>
								Voltar
							</p>
							<form onSubmit={create}>
								<div className="fieldWrapper" key={0}>
									<label>
										<p>Projeto</p>
									</label>
									<select
										// defaultValue={edit.edit ? itemEdit[field.value] : "default"}
										className="formField"
										name="id_projeto"
										onChange={(e) => handleChange(e)}
										onBlur={(e) => handleChange(e)}
									>
										{generateOptions(projeto)}
									</select>
								</div>
								<div className="fieldWrapper" key={1}>
									<label>
										<p>Usuario</p>
									</label>
									<select
										// defaultValue={edit.edit ? itemEdit[field.value] : "default"}
										className="formField"
										name="id_usuario"
										onChange={(e) => handleChange(e)}
										onBlur={(e) => handleChange(e)}
									>
										{generateOptions(usuario)}
									</select>
								</div>
								<input
									disabled={hasEmptyFields}
									id="submitButton"
									type="submit"
									value="Criar"
								/>
							</form>
						</ContainerForm>
					</Container>
				) : (
					<>
						<ModalUsuarios
							isOpen={modal}
							onClose={onCloseModal}
							usuarios={users}
						/>
						<Link style={{ textDecoration: "none" }} to="/menu">
							<p className="whiteText">Voltar</p>
						</Link>
						<AppTitle>{config.texts.title}</AppTitle>
						<button id="newItemButton" onClick={newItem}>
							+ Nova relação
						</button>
						<TableWrapper styles={config.styles}>
							{!data.length && columns ? (
								<div className="loadingText">Carregando...</div>
							) : (
								<Table columns={columns} data={data} />
							)}
						</TableWrapper>
					</>
				)}
			</Container>
		</>
	);
};

export default withRouter(ProjetoUsuario);
