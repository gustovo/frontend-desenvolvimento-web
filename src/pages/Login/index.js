import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import { Form, Container, AppTitle } from "./styles";
import { login } from "../../services/auth";
import api from "../../services/api";

class Login extends Component {
	state = {
		username: "",
		password: "",
		error: "",
	};

	handleLogin = async (e) => {
		e.preventDefault();
		const { username } = this.state;
		if (!username) {
			this.setState({
				error: "Favor informar usuário",
			});
		} else {
			try {
				await api.post(`/login?user=${username}`).then((r) => {
					if (r.status == 200) {
						login(r.data.token);
						localStorage.setItem("user", r.data.user);
					} else
						this.setState({
							error: "Usuário inválido!",
						});
				});
				this.props.history.push("/menu");
			} catch (err) {
				this.setState({
					error: "Usuário inválido!",
				});
			}
		}
	};

	render() {
		return (
			<Container>
				<AppTitle>TaskManager</AppTitle>
				<Form onSubmit={this.handleLogin}>
					{this.state.error && <p>{this.state.error}</p>}
					<div>
						<p>Usuário</p>
						<input
							type="text"
							onChange={(e) => this.setState({ username: e.target.value })}
						/>
					</div>
					<button type="submit">Entrar</button>
				</Form>
			</Container>
		);
	}
}
export default withRouter(Login);
