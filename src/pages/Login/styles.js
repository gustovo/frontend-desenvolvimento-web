import styled from "styled-components";
import theme from "../../styles/theme";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

export const Form = styled.form`
  width: 400px;
  background: ${theme.bgColor};
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  img {
    width: 100px;
    margin: 10px 0 40px;
  }
  p {
    font-size: 18px;
    color: ${theme.mainGreen};
    padding: 10px;
    width: 100%;
    text-align: center;
  }
  div {
    width: 100%;
  }
  input {
    border-radius: 50px;
    flex: 1;
    height: 46px;
    margin-bottom: 15px;
    padding: 0;
    color: #9d9d9d;
    background-color: #202020;
    padding-left: 20px;
    font-size: 15px;
    width: 100%;
    border-style: solid;
    border-color: #8080805e;
    border-width: 1px;
    &::placeholder {
      color: #999;
    }
  }
  button {
    color: #fff;
    font-size: 18px;
    margin-top: 20px;
    background: ${theme.mainGreen};
    height: 50px;
    border: 0;
    border-radius: 40px;
    width: 70%;
  }
  hr {
    margin: 20px 0;
    border: none;
    border-bottom: 1px solid #cdcdcd;
    width: 100%;
  }
  a {
    margin-top: 20px;
    font-size: 16;
    font-weight: bold;
    color: #999;
    text-decoration: none;
  }
`;

export const AppTitle = styled.p`
    font-size: 104px;
    color: ${theme.mainGreen}

`