import styled from "styled-components";
import theme from "../../styles/theme"

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
`;


export const MenuOption = styled.button`
    border: none;
    background-color: ${theme.mainGreen};
    border-radius: 50px;
    /* height: 5vh; */
    width: 15vw;
    margin-bottom: 1vh;

    p {
      text-align: center;
      padding: 15px;
    }
`;

export const AppTitle = styled.p`
    font-size: 104px;
    color: ${theme.mainGreen};
    margin-bottom: 35px;

`