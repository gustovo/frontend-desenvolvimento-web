export const TOKEN = "credentials";
export const isAuthenticated = () => {
  return localStorage.getItem(TOKEN);
};
export const getToken = () => localStorage.getItem(TOKEN);
export const login = (token) => {
  localStorage.setItem(TOKEN, token);
};
export const logout = () => {
  localStorage.removeItem(TOKEN);
};
